/**
 * Created by taras on 29.05.16.
 */

import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Main from './components/main/Main';
import ItemPage from './components/item/ItemPage';
import ItemPageRead from './components/item/ItemPageRead';
import TemplatePage from './components/common/TemplatePage';

export default (
    <Route path="/" component={TemplatePage}>
        <IndexRoute component={Main} />
        <Route path="read/:id" component={ItemPageRead} />
        <Route path="edit/:id" component={ItemPage} />
    </Route>
);
