import React from 'react';
import {deepOrange500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Table from '../common/TablePage';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import UserAdd from 'material-ui/svg-icons/content/add';
import {connect} from 'react-redux';
import * as itemActions from '../../actions/itemActions';
import {Map} from 'immutable';

const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 20
  }
};

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500
  }
});

const uid = () => Math.random().toString(34).slice(2);

class Main extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
  }

  addItem(event) {
    event.preventDefault();
    const newId = uid();
    let item = Map({id: newId, title: 'no name', status: '', date: ''});
    this.props.dispatch(itemActions.addItem(item));
  }

  removeItem(event){
    const index = event.target.id;
    this.props.dispatch(itemActions.deleteItem(index));
  }

  render() {

    const style = {
      marginRight: 20
    };

    const {items} = this.props;
    
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div style={styles.container}>
          <h1>CRUD Table</h1>
          <Table items={items} remove={this.removeItem}/>
            <FloatingActionButton
                style={style}
                onTouchTap={this.addItem}>
              <UserAdd />
            </FloatingActionButton>
        </div>
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    items: state.items
  };

}

export default connect(mapStateToProps)(Main);
