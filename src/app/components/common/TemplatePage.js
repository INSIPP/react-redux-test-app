/**
 * Created by taras on 29.05.16.
 */

import React, {PropTypes} from 'react';

class TemplatePage extends React.Component {
    render() {
        return (
            <div >
                {this.props.children}
            </div>
        );
    }
}

TemplatePage.propTypes = {
    children: PropTypes.object.isRequired
};

export default TemplatePage;