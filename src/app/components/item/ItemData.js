/**
 * Created by taras on 30.05.16.
 */

import React from 'react';

const ItemData = ({item}) => {
    return (
        <div>
            <p><b>Title: </b>{item.get('title')}</p>
            <p><b>Status: </b>{item.get('status')}</p>
            <p><b>Date: </b>{item.get('date')}</p>

        </div>
    );
};

ItemData.propTypes = {
    item: React.PropTypes.object.isRequired
};

export default ItemData;