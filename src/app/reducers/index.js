/**
 * Created by taras on 29.05.16.
 */

import {combineReducers} from 'redux';
import items from './itemReducer';

const rootReducer = combineReducers({
    items
});

export default rootReducer;
