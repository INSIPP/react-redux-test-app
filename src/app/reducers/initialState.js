/**
 * Created by taras on 29.05.16.
 */

import { List, Map } from 'immutable';

export default {
    items : List([
        Map({id:'1',title:'name1', status:'active', date:'date1'}),
        Map({id:'2', title:'name2', status:'disable', date:'date2'})
    ])
}